angular.module("callboardApp", ['ngRoute', 'ngFileSaver'])

    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: 'src/scripts/templates/login.html'
            })
            .when("/notes", {
                templateUrl: 'src/scripts/templates/notes.html',
                controller: 'NotesController'
            })
    })

    .run(function ($location, $rootScope, RouteFilter, AuthService, AUTH_EVENTS) {

        $rootScope.$on('$locationChangeStart', function(scope, next, current) {
            RouteFilter.run($location.path());
        });

        $rootScope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
            AuthService.logout();
            alert('Sorry, your session is lost!');
        });

    });