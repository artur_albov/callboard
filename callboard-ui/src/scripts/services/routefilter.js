angular.module('callboardApp')

    .service('RouteFilter', function($location) {

        var filters = [];

        var getFilter = function(route) {
            for(var i = 0; i < filters.length; i++) {
                for(var j = 0; j < filters[i].routes.length; j++) {
                    if(matchRoute(filters[i].routes[j], route)) {
                        return filters[i];
                    }
                }
            }
        };

        var matchRoute = function(filterRoute, route) {
            if(route instanceof RegExp) {
                return filterRoute.test(route);
            } else {
                return route === filterRoute;
            }
        };



        return {

            register: function(name, routes, callback, redirectUrl) {
                var redirectUrl = typeof redirectUrl !== 'undefined' ? redirectUrl : null;
                filters.push({
                    name: name,
                    routes: routes,
                    callback: callback,
                    redirectUrl: redirectUrl
                })
            },

            run: function(route) {
                var filter = getFilter(route);
                if(filter != null && filter.redirectUrl != null) {
                    if(!filter.callback()) {
                        $location.path(filter.redirectUrl);
                    }
                } else {
                    $location.path('/');
                }
            }
        }


    });