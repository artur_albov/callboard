angular.module('callboardApp')

    .service('AuthService', function($q, $http, $location, API_ENDPOINT, AUTH) {
        var LOCAL_LOGIN_KEY = 'callboardUserLogin';
        var LOCAL_TOKEN_KEY = 'callboardJwt';
        var isAuthenticated = false;
        var userLogin;

        function loadUserCredentials() {
            var login = window.localStorage.getItem(LOCAL_LOGIN_KEY);
            var jwt = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (login) {
                useCredentials(login, jwt);
            }
        }

        function storeUserCredentials(user) {
            window.localStorage.setItem(LOCAL_LOGIN_KEY, user.login);
            window.localStorage.setItem(LOCAL_TOKEN_KEY, user.jwt);
            useCredentials(user.login, user.jwt);
        }

        function useCredentials(login, jwt) {
            isAuthenticated = true;
            userLogin = login;
            $http.defaults.headers.common[AUTH.tokenHeader] = jwt;
        }


        function destroyUserCredentials() {
            userLogin = undefined;
            isAuthenticated = false;
            window.localStorage.removeItem(LOCAL_LOGIN_KEY);
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            $http.defaults.headers.common[AUTH.tokenHeader] = undefined;
        }

        var login = function(user) {
            return $q(function(resolve, reject) {
                $http.post(API_ENDPOINT.url + '/user/checkPin', user).then(function(result) {
                    if(result.data.error != null) {
                        reject(result.data.error);
                    } else {
                        storeUserCredentials(result.data.payback);
                        resolve(result.data.payback);
                    }
                });
            });
        };

        var logout = function() {
            destroyUserCredentials();
            $location.path('/');
        };

        loadUserCredentials();

        return {
            login: login,
            logout: logout,
            isAuthenticated: function() {return isAuthenticated;},
            getUserLogin: function() {
                return userLogin;
            }
        };
    })

    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });