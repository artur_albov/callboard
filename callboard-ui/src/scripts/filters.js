angular.module('callboardApp')

    .run(function(RouteFilter, AuthService) {


        RouteFilter.register('all', ['/notes'], function() {
            return AuthService.isAuthenticated();
        }, '/');


        RouteFilter.register('all', ['/'], function() {
            return !AuthService.isAuthenticated();
        }, '/notes');

    });