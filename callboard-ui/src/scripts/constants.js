angular.module('callboardApp')

    .constant('API_ENDPOINT', {
        url: 'http://localhost:3000'
    })

    .constant('AUTH', {
        tokenHeader: 'X-Auth-Token'
    })

    .constant('AUTH_EVENTS', {
        notAuthenticated: 'auth-not-authenticated'
    });