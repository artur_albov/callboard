angular.module("callboardApp")

.controller('LoginController', function($scope, $rootScope, $http, $location, AuthService, API_ENDPOINT) {
    $scope.loading = false;
    $scope.user = {
        login: '',
        pin: ''
    };

    $scope.loginErrors = '';

    $scope.modalShown = false;

    $scope.loginPin = function () {
        $scope.loading = true;
        AuthService.login($scope.user).then(function () {
            $location.path('/notes');
            $scope.loading = false;
        }, function (errMsg) {
            $scope.authErrors = errMsg;
            $scope.loading = false;
        });
        $scope.modalShown = !$scope.modalShown;
    };

    $scope.login = function () {
        $scope.loading = true;
        $http.post(API_ENDPOINT.url + '/user/login', $scope.user).then(function (result) {
            if(!result.data.error) {
                $scope.modalShown = !$scope.modalShown;
            } else {
                $scope.authErrors = result.data.error;
            }
            $scope.loading = false;
        });
    }
})

.directive('loading', function() {
    return {
        restrict: 'E',
        template: '<div class="loading-overlay"><div class="loading"><img src="../../../loading.gif"/></div></div>',
        replace: true
    }
});