angular.module("callboardApp")

    .controller('NotesController', function($scope, $rootScope, $http, AuthService, API_ENDPOINT) {
        $scope.loading = false;
        $scope.userLogin = AuthService.getUserLogin();

        $scope.note={};


        $scope.updateNotes = function($fileContent){
            $scope.loading = true;
            $scope.key = $fileContent;
            $http.post(API_ENDPOINT.url + '/note/notes', $scope.key).then(function(result) {
                if(result.data.error == null) {
                    $scope.enc = CryptoJS.AES.encrypt(JSON.stringify(result.data.payback), $scope.key);
                    window.localStorage.setItem("Notes", $scope.enc);
                }
                else{
                    alert(result.data.error);
                }
                $scope.loading = false;
            });
        };

        $scope.showNotes = function($fileContent){
            $scope.loading = true;
            $scope.key = $fileContent;
            $http.post(API_ENDPOINT.url + '/note/notes', $scope.key).then(function (result) {
                if(result.data.error == null) {
                    $scope.notes = JSON.parse(CryptoJS.AES.decrypt(window.localStorage.getItem("Notes"), $scope.key).toString(CryptoJS.enc.Utf8));
                }
                else{
                    alert(result.data.error);
                }
                $scope.loading = false;
            });
        };

        $scope.logout = function () {
            AuthService.logout();
        };

        $scope.modalShown1 = false;
        $scope.modalShown2 = false;

        $scope.openEditDialog = function(note) {
            $scope.note = note;
            $scope.noteToEdit = JSON.parse(JSON.stringify(note));
            $scope.modalShown1 = !$scope.modalShown1;
        };

        $scope.openShareDialog = function(note) {
            $scope.note = note;
            $scope.noteToShare = JSON.parse(JSON.stringify(note));
            $scope.modalShown2 = !$scope.modalShown2;
        };

        $scope.changeNote = function(){
            $scope.loading = true;
            $http.post(API_ENDPOINT.url + '/note/changeNote', $scope.noteToEdit).then(function(result){
                $scope.loading = false;
                $scope.modalShown1 = !$scope.modalShown1;
            });
        };

        $scope.shareNote = function(){
            $scope.loading = true;
            $http.post(API_ENDPOINT.url+'/note/shareNote', {noteId: $scope.noteToShare.id, userLogin: $scope.noteToShare.nick}).then(
                function() {
                    $scope.loading = false;
                    $scope.modalShown2 = !$scope.modalShown2;
                });
        };
    })

    .directive('onReadFile', function ($parse) {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {
                var fn = $parse(attrs.onReadFile);
                element.on('change', function(onChangeEvent) {
                    var reader = new FileReader();

                    reader.onload = function(onLoadEvent) {
                        scope.$apply(function() {
                            fn(scope, {$fileContent:onLoadEvent.target.result});
                        });
                    };

                    reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
                });
            }
        };
    })

    .directive('modalDialog', function() {
    return {
        restrict: 'E',
        scope: {
            show: '=',
            noteSrc: '='
        },
        replace: true,
        windowClass: 'center-modal',
        transclude: true,
        link: function(scope, element, attrs) {
            scope.dialogStyle = {};

            if (attrs.width) {
                scope.dialogStyle.width = attrs.width;
            }

            if (attrs.height) {
                scope.dialogStyle.height = attrs.height;
            }

            scope.hideModal = function() {
                scope.show = false;
            };
        },
        template:
        "<div class='ng-modal' ng-show='show'>"+
            "<div class='ng-modal-overlay' ng-click='hideModal()'></div>"+
                "<div class='ng-modal-dialog' ng-style='dialogStyle'>"+
                "<button type='button' class='close ng-modal-close' ng-click='hideModal()'>&times;</button>"+
                "<div class='ng-modal-dialog-content' ng-transclude></div>"+
            "</div>"+
        "</div>"
    };

});