package by.albov.filter;

import by.albov.AppConstants;
import by.albov.model.User;
import by.albov.repository.UserRepository;
import by.albov.service.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by a.albov on 26.10.2016.
 */
public class AuthFilter implements Filter {

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        tokenUtils = WebApplicationContextUtils
                .getRequiredWebApplicationContext(filterConfig.getServletContext())
                .getBean(TokenUtils.class);
        userRepository = WebApplicationContextUtils
                .getRequiredWebApplicationContext(filterConfig.getServletContext())
                .getBean(UserRepository.class);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse) response;

        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        resp.setHeader("Access-Control-Max-Age", "3600");
        resp.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, " + AppConstants.tokenHeader);

        HttpServletRequest httpRequest = (HttpServletRequest) request;

        if(!httpRequest.getMethod().equals("OPTIONS")) {
            String authToken = httpRequest.getHeader(AppConstants.tokenHeader);
            String username = tokenUtils.getUsername(authToken);

            if(username != null) {
                User user = userRepository.findOne(username);
                if(tokenUtils.validateToken(authToken, user)) {
                    chain.doFilter(request, response);
                } else {
                    resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            } else {
                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
