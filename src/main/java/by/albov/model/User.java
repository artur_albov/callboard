package by.albov.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by artur on 13.10.16.
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@Data
public class User {
    @Id
    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false)
    private String mail;

    @Column(name = "pin")
    private Integer pin;

    @Column(name = "key", nullable = false)
    private String key;

    @Column(name = "pinLifetime")
    private Long pinLifetime;

    @ManyToMany(mappedBy = "users", cascade = CascadeType.ALL)
    private List<Note> notes;

    public User(String login, String password, String mail) {
        this.login = login;
        this.mail = mail;
        this.password = password;
    }

    public User(String login, String password, String mail, String key){
        this.login = login;
        this.mail = mail;
        this.password = password;
        this.key = key;
    }

}
