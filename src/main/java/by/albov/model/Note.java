package by.albov.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by artur on 13.10.16.
 */
@Entity
@Table(name = "notes")
@Data
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "content", nullable = false, length = 1000)
    private String content;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "version")
    private int version;

    public Note() {
        version = 1;
    }


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_note_link",
            joinColumns = @JoinColumn(name = "note_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_login", referencedColumnName = "login"))
    private List<User> users;
}
