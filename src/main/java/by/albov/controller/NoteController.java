package by.albov.controller;

import by.albov.AppConstants;
import by.albov.dto.NoteDTO;
import by.albov.dto.ShareDTO;
import by.albov.model.Note;
import by.albov.model.User;
import by.albov.repository.NoteRepository;
import by.albov.repository.UserRepository;
import by.albov.service.EncryptionUtils;
import by.albov.service.NoteService;
import by.albov.service.TokenUtils;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by artur on 13.10.16.
 */
@RestController
@CrossOrigin
@RequestMapping("/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/notes", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse getNotes(@RequestBody String key, HttpServletRequest request) throws IOException {
        String authToken = request.getHeader(AppConstants.tokenHeader);
        String username = tokenUtils.getUsername(authToken);
        User user = userRepository.findByUsername(username);
        if(!BCrypt.checkpw(key, user.getKey()))
            return RestResponse.error("Bad key");
        List<NoteDTO> noteDTOs = noteService.getNotes();
        BufferedReader reader = new BufferedReader(new FileReader("D:\\key.txt"));
        String myKey = reader.readLine();
        for(NoteDTO temp : noteDTOs){
            temp.setContent_enc(EncryptionUtils.decrypt(myKey,temp.getContent_enc()));
            temp.setTitle(EncryptionUtils.decrypt(myKey, temp.getTitle()));
        }
        return RestResponse.ok(noteDTOs);
    }

    @RequestMapping(value = "/noteInfo", method = RequestMethod.GET)
    public RestResponse getNote(@RequestParam String noteID) throws IOException {
        NoteDTO noteDTO = new NoteDTO(noteService.getOne(Long.parseLong(noteID)));
        return RestResponse.ok(noteDTO);
    }


    @RequestMapping(value = "/changeNote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse changeNote(@RequestBody NoteDTO noteDTO, HttpServletRequest request) throws Exception {
        String authToken = request.getHeader(AppConstants.tokenHeader);
        String username = tokenUtils.getUsername(authToken);
        Note note = noteService.getOne(noteDTO.getId());

        if(!note.getUsers().stream().map(User::getLogin).collect(Collectors.toSet()).contains(username))
            return RestResponse.error("This user can't do this");
        if(note.getVersion() != noteDTO.getVersion())
            return RestResponse.error("Version changed");

	    note.setVersion(noteDTO.getVersion()+1);
        note.setContent(noteDTO.getContent_enc());
        note.setTitle(noteDTO.getTitle());

        return RestResponse.ok(noteService.convertToDTO(noteService.save(note)));
    }

    @RequestMapping(value = "/shareNote", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse shareNote(@RequestBody ShareDTO noteDTO, HttpServletRequest request) throws IOException {
        String authToken = request.getHeader(AppConstants.tokenHeader);
        String username = tokenUtils.getUsername(authToken);
        if(!noteService.getOne(noteDTO.getNoteId()).getUsers().stream()
                .map(User::getLogin).collect(Collectors.toSet()).contains(username))
            return RestResponse.error("This user can't do this");
        Note note = noteService.getOne(noteDTO.getNoteId());
        if(noteDTO.getUserLogin() != null) {
            User user = userRepository.findOne(noteDTO.getUserLogin());
            if(user == null) {
                return RestResponse.error("Such user doesn't exists");
            }
            if(!note.getUsers().stream().map(User::getLogin).collect(Collectors.toSet()).contains(user.getLogin())) {
                note.getUsers().add(user);
                return RestResponse.ok(noteService.convertToDTO(noteService.save(note)));
            } else {
                return RestResponse.error("This user already has access");
            }
        }
        return RestResponse.error("Empty user");
    }
}
