package by.albov.controller;

import by.albov.dto.UserDTO;
import by.albov.model.Note;
import by.albov.model.User;
import by.albov.repository.UserRepository;
import by.albov.service.NoteService;
import by.albov.service.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * Created by artur on 13.10.16.
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NoteService noteService;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TokenUtils tokenUtils;

    @RequestMapping(value = "/checkPin", method = RequestMethod.POST,  consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse checkPin(@RequestBody UserDTO userDto){
        User user = userRepository.findOne(userDto.getLogin());
        Set set = new HashSet();
        set.add(1);
        if(user.getPinLifetime()<System.currentTimeMillis()){
            user.setPin(-1);
            user.setPinLifetime(0L);
            return RestResponse.error("Lifetime of pin expire");
        }
        if(userDto.getPin() == -1)
            return RestResponse.error("Bad pin");
        if(!user.getPin().equals(userDto.getPin())) {
            user.setPin(-1);
            user.setPinLifetime(0L);
            return RestResponse.error("Pin doesn't match");
        }
        user.setPin(-1);
        user.setPinLifetime(0L);
        return RestResponse.ok(new UserDTO(user, tokenUtils.createToken(user)));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse login(@RequestBody UserDTO userDTO) {
        User user = userRepository.findOne(userDTO.getLogin());
        if(user == null) {
            return RestResponse.error("User doesn't exists");
        }
        if(!BCrypt.checkpw(userDTO.getPassword(), user.getPassword())){
            return RestResponse.error("Password doesn't match");
        }
        Random r = new Random(System.currentTimeMillis());
        int pin = r.nextInt(8999)+1000;
        userRepository.findOne(userDTO.getLogin()).setPin(pin);
        userRepository.findOne(userDTO.getLogin()).setPinLifetime(System.currentTimeMillis() + 60*1000);
        userRepository.flush();
        /*sendMail(user);*/
        return RestResponse.ok("ok");
    }

    private void sendMail(User user){
        MimeMessage mail = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getMail());
            helper.setText(String.valueOf(user.getPin()));
            helper.setSubject("Pin");
            helper.setReplyTo("xxxxotabxxx@gmail.com");
            helper.setFrom("xxxxotabxxx@gmail.com");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        javaMailSender.send(mail);
    }


    @RequestMapping(value = "/generate")
    public String generate() throws Exception {
        User user1 = new User("artur", BCrypt.hashpw("test", BCrypt.gensalt(12)), "xxxxotabxxx@mail.ru", BCrypt.hashpw("ArturKey", BCrypt.gensalt(12)));
        User user2 = new User("alice", BCrypt.hashpw("test", BCrypt.gensalt(12)), "artur.albov@gmail.com", BCrypt.hashpw("AliceKey", BCrypt.gensalt(12)));
        User user3 = new User("bob", BCrypt.hashpw("test", BCrypt.gensalt(12)), "artur.albov@gmail.com", BCrypt.hashpw("BobKey", BCrypt.gensalt(12)));
        User user4 = new User("victor", BCrypt.hashpw("test", BCrypt.gensalt(12)), "artur.albov@gmail.com", BCrypt.hashpw("VictorKey", BCrypt.gensalt(12)));
        User user5 = new User("stas", BCrypt.hashpw("test", BCrypt.gensalt(12)), "xxxxotabxxx@mail.ru", BCrypt.hashpw("StasKey", BCrypt.gensalt(12)));
        user1.setPin(0);
        user2.setPin(0);
        Note note1 = new Note();
        note1.setTitle("Javascript");
        note1.setContent("JavaScript extension to the ECMA-262 standard, as such it may not be present in other implementations of the standard.");
        System.out.println(note1.getContent());
        note1.setUsers(Collections.singletonList(user1));
        Note note2 = new Note();
        note2.setTitle("Similar functionality");
        note2.setContent("But you can add similar functionality to arrays in Internet Explorer (and other browsers that don't suppor");
        note2.setUsers(Collections.singletonList(user1));
        Note note3 = new Note();
        note3.setTitle("Salmon");
        note3.setContent("A large edible fish that is a popular game fish, much prized for its pink flesh. Salmon mature in the sea but migrate to freshwater streams to spawn.");
        note3.setUsers(Collections.singletonList(user2));
        Note note4 = new Note();
        note4.setTitle("With my them");
        note4.setContent("With my them if up many. Lain week nay she them her she. Extremity so attending objection as engrossed gentleman something.");
        note4.setUsers(Collections.singletonList(user3));
        Note note5 = new Note();
        note5.setTitle("Instantly gentleman");
        note5.setContent("Instantly gentleman contained belonging exquisite now direction she ham. West room at sent if year. Numerous indulged distance old law you. ");
        note5.setUsers(Collections.singletonList(user4));
        Note note6 = new Note();
        note6.setTitle("Total state");
        note6.setContent("Total state as merit court green decay he. Steepest sex bachelor the may delicate its yourself. ");
        note6.setUsers(Collections.singletonList(user5));
        Note note7 = new Note();
        note7.setTitle("In no impression");
        note7.setContent("In no impression assistance contrasted. Manners she wishing justice hastily new anxious. At discovery discourse departure objection we.   ");
        note7.setUsers(Collections.singletonList(user1));
        Note note8 = new Note();
        note8.setTitle("Cordially");
        note8.setContent("Cordially convinced did incommode existence put out suffering certainly. Besides another and saw ferrars limited ten say unknown.");
        note8.setUsers(Collections.singletonList(user2));
        Note note9 = new Note();
        note9.setTitle("Call park");
        note9.setContent("Call park out she wife face mean. Invitation excellence imprudence understood it continuing to. Ye show done an into.");
        note9.setUsers(Collections.singletonList(user1));
        Note note10 = new Note();
        note10.setTitle("Fifteen winding");
        note10.setContent("Fifteen winding related may hearted colonel are way studied. County suffer twenty or marked no moment in he. Meet shew or said like he. Valley silent cannot things so remain oh to elinor.");
        note10.setUsers(Collections.singletonList(user2));
        noteService.save(note1);
        noteService.save(note2);
        noteService.save(note3);
        noteService.save(note4);
        noteService.save(note5);
        noteService.save(note6);
        noteService.save(note7);
        noteService.save(note8);
        noteService.save(note9);
        noteService.save(note10);
        return "ok";
    }
}
