package by.albov.controller;

/**
 * Created by artur on 13.10.16.
 */
public class RestResponseError<ERROR> extends RestResponse<Object, ERROR> {

    RestResponseError(Object o, ERROR error) {
        super(o, error);
    }
}
