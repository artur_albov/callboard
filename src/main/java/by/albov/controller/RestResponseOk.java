package by.albov.controller;

/**
 * Created by artur on 13.10.16.
 */
public class RestResponseOk<PAYBACK> extends RestResponse<PAYBACK, Object> {

    RestResponseOk(PAYBACK payback, Object o) {
        super(payback, o);
    }
}
