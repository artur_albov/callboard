package by.albov.repository;

import by.albov.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by artur on 13.10.16.
 */
@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
    @Query("select n from Note n order by n.id asc ")
    List<Note> findAllOrderByIdAsc();
}
