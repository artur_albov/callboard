package by.albov.repository;

import by.albov.model.Note;
import by.albov.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by artur on 13.10.16.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
    @Query("select n from User n where n.login=:username ")
    User findByUsername(@Param("username") String username);
}
