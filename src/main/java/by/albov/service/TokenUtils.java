package by.albov.service;

import by.albov.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Stas on 25.10.2016.
 */

@Component
public class TokenUtils {
    private final String SECRET = "!A3v(rHc7fd'q~uc_l3N4a4jf1M+q4";

    public boolean validateToken(String jwt, User user) {
        final String username = getUsername(jwt);
        final String password = getPassword(jwt);
        return (username.equals(user.getLogin()) && !isTokenExpired(jwt) && password.equals(user.getPassword()));
    }

    public String getUsername(String token) {
        String username;
        try {
            final Claims claims = getClaimsFromToken(token);
            username = (String) claims.get("login");
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public String getPassword(String token){
        String password;
        try{
            final Claims claims = getClaimsFromToken(token);
            password = (String) claims.get("password");
        }catch (Exception e){
            password = null;
        }
        return password;
    }

    private Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = this.getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = this.getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public String createToken(User user){
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("login", user.getLogin());
        claims.put("password", user.getPassword());
        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + 60*60*1000);
    }

    public Boolean canTokenBeRefreshed(String token) {
        return !isTokenExpired(token);
    }

    public String refreshToken(String token) {
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }
}
