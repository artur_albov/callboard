package by.albov.service;

import by.albov.dto.NoteDTO;
import by.albov.model.Note;
import by.albov.model.User;
import by.albov.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Stas on 13.10.2016.
 */

@Transactional
@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepository;

    public Note save(Note note) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("D:\\key.txt"));
        String key = reader.readLine();
        note.setTitle(EncryptionUtils.encrypt(key, note.getTitle()));
        note.setContent(EncryptionUtils.encrypt(key, note.getContent()));
        return noteRepository.saveAndFlush(note);
    }

    public List<NoteDTO> getNotes() throws IOException {
        List<Note> notes = noteRepository.findAllOrderByIdAsc();
        return getNoteDTO(notes);
    }

    public Note getOne(Long id) throws IOException {
        Note note = noteRepository.findOne(id);
        note.getUsers();
        BufferedReader reader = new BufferedReader(new FileReader("D:\\key.txt"));
        String key = reader.readLine();
        note.setTitle(EncryptionUtils.decrypt(key, note.getTitle()));
        note.setContent(EncryptionUtils.decrypt(key, note.getContent()));
        return note;
    }

    private List<NoteDTO> getNoteDTO(List<Note> notes){
        return notes.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    public NoteDTO convertToDTO(Note note){
        NoteDTO nd = new NoteDTO(note.getId(), note.getTitle(), note.getContent(), note.getVersion());
        for(User user: note.getUsers()) {
            nd.getUsers().add(user.getLogin());
        }
        return nd;
    }
}
