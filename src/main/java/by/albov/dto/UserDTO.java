package by.albov.dto;

import by.albov.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by artur on 13.10.16.
 */
@NoArgsConstructor
public class UserDTO {
    @Getter
    private String login;
    @Getter
    private String password;
    @Getter
    private Integer pin;
    @Getter
    private String jwt;

    public UserDTO(User user, String jwt){
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.pin = user.getPin();
        this.jwt = jwt;
    }
}
