package by.albov.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by a.albov on 13.10.2016.
 */
@NoArgsConstructor
@AllArgsConstructor
public class ShareDTO {
    @Getter
    private Long noteId;
    @Getter
    private String userLogin;
}
