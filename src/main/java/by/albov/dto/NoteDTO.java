package by.albov.dto;

import by.albov.model.Note;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stas on 13.10.2016.
 */
@NoArgsConstructor
@AllArgsConstructor
public class NoteDTO {
    @Getter
    private Long id;
    @Getter
    @Setter
    private String content_enc;
    @Getter
    @Setter
    private String title;
    @Getter
    private int version;
    @Getter
    private List<String> users = new ArrayList<>();

    public NoteDTO(Long id, String title, String content_enc, int version) {
        this.id = id;
        this.title = title;
        this.content_enc = content_enc;
        this.version = version;
    }

    public NoteDTO(Note note){
        this.id = note.getId();
        this.title = note.getTitle();
        this.content_enc = note.getContent();
        this.version = note.getVersion();
    }
}
